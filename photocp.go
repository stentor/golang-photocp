package main

import (
	"flag"
	"fmt"
	"github.com/rwcarlsen/goexif/exif"
	"io"
	"io/ioutil"
	"os"
	"path"
	"runtime"
)

func ResolveTemplateToSubdir(template string, exif *exif.Exif) string {
	var subdir string
	tm, _ := exif.DateTime()
	y, m, d := tm.Date()
	manufacturer, _ := exif.Get("Make")
	model, _ := exif.Get("Model")
	for i := 0; i < len(template); i++ {
		switch template[i] {
		case 'y':
			subdir += fmt.Sprintf("%d", y)
		case 'm':
			subdir += fmt.Sprintf("%02d", m)
		case 'M':
			subdir += fmt.Sprintf("%s", m)
		case 'd':
			subdir += fmt.Sprintf("%02d", d)
		case 'F':
			if manufacturer != nil {
				manufact_str, _ := manufacturer.StringVal()
				subdir += fmt.Sprintf("%s", manufact_str)
			}
		case 'T':
			if model != nil {
				model_str, _ := model.StringVal()
				subdir += fmt.Sprintf("%s", model_str)
			}
		case '/':
			subdir += "/"
		default:
			subdir += string(template[i])
		}
	}
	return subdir
}

func CreateExifDir(basedir string, template string, exif *exif.Exif) string {
	subdir := ResolveTemplateToSubdir(template, exif)
	new_dir := path.Join(basedir, subdir)
	os.MkdirAll(new_dir, os.ModePerm)

	return new_dir
}

func OpenFile(file string) *os.File {
	f, err := os.Open(file)
	if err != nil {
		panic(err)
	}
	return f
}

func CreateFile(file string) *os.File {
	f, err := os.Create(file)
	if err != nil {
		panic(err)
	}
	return f
}

func CopyFileToDir(file string, target_dir string) {
	dst_path := path.Join(target_dir, path.Base(file))
	fmt.Printf("Copy [%s] -> [%s] \n", file, dst_path)
	src_f := OpenFile(file)
	dst_f := CreateFile(dst_path)

	defer src_f.Close()
	defer dst_f.Close()

	_, err := io.Copy(dst_f, src_f)
	if err != nil {
		panic(err)
	}
}

func CopyExifFile(basedir string, template string, file string) {
	f, err := os.Open(file)
	if err != nil {
		panic(err)
	}
	exif_info, err := exif.Decode(f)
	if err != nil {
		fmt.Printf("Cannot decode EXIF : [%s]\n", file)
		f.Close()
		return
	}
	f.Close()
	dir := CreateExifDir(basedir, template, exif_info)
	CopyFileToDir(file, dir)
}

func CopyExifFilesFromDir(dir string, template string, target_basedir string) {
	numCpu := runtime.NumCPU()
	semaphore := make(chan bool, numCpu)
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		panic(err)
	}
	for _, file := range files {
		fpath := path.Join(dir, file.Name())
		if file.IsDir() {
			CopyExifFilesFromDir(fpath, template, target_basedir)
		} else {
			semaphore <- true
			go func(target_basedir string, template string, fpath string) {
				CopyExifFile(target_basedir, template, fpath)
				defer func() { <-semaphore }()
			}(target_basedir, template, fpath)
		}
	}
	for i := 0; i < cap(semaphore); i++ {
		semaphore <- true
	}
}

func help() {
	fmt.Println("Copy photos from src_dir to target_dir/TEMPLATE by date and manufacturer/model stored in the EXIF header")
	fmt.Println("TEMPLATE is a pattern where you can define the directory layout.")
	fmt.Println("   y : year")
	fmt.Println("   m : month as number with a leading zero")
	fmt.Println("   M : month as long name(eg. 'December')")
	fmt.Println("   d : day as number with a leading zero")
	fmt.Println("   F : manufacturer of the camera")
	fmt.Println("   T : model/type of the camera")
	fmt.Println("")
	fmt.Println("Example: ")
	fmt.Println("TEMPLATE : 'y/M/ymd/F-T'")
	fmt.Println("result   : 2016/December/20161210/Panasonic-DMC-LX100/P1260369.JPG")
}

func main() {
	var src_dir string
	var template string
	var target_dir string
	flag.StringVar(&src_dir, "s", "", "Source directory")
	flag.StringVar(&target_dir, "t", "", "Target directory")
	flag.StringVar(&template, "T", "y/M/ymd/F-T", "Template")
	flag.Parse()
	if src_dir == "" || target_dir == "" {
		help()
		fmt.Println("Usage:")
		flag.PrintDefaults()
		return
	}
	fmt.Printf("src_dir[%s], target_dir[%s]\n", src_dir, target_dir)
	CopyExifFilesFromDir(src_dir, template, target_dir)
}
